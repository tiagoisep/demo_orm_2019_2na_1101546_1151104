/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_ORM.apresentacao;

import isep.eapli.demo_ORM.aplicacao.RegistarAutomovelController;
import isep.eapli.demo_ORM.util.Console;
import isep.eapli.demo_orm.dominio.Automovel;

/**
 *
 * @author Ferreira
 */
public class RegistarAutomovelUI {
 
       private final RegistarAutomovelController controller = new RegistarAutomovelController();
    
    public void registarAutomovel() {
        System.out.println("*** Registo  Automóvel ***\n");
        String marca = Console.readLine("Marca:");
        String cor = Console.readLine("Cor");
        int cilindrada = Console.readInteger("Cilindrada:");
        int matricula = Console.readInteger("Matricula:");
        double km = Console.readDouble("Km's:");
        Automovel automovel = controller.registaAutomovel(marca, cor, cilindrada, matricula, km);
        System.out.println("Automóvel" + automovel);
    }
    
}

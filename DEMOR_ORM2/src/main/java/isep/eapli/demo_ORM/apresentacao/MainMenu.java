/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_ORM.apresentacao;

import isep.eapli.demo_ORM.util.Console;
import isep.eapli.demo_orm.apresentacao.GrupoAutomovelUI;

/**
 *
 * @author mcn
 */
public class MainMenu {

    public static void mainLoop() {
        int opcao = 0;
        do {
            opcao = menu();
            //A fechar issues
            switch (opcao) {
                case 0:
                    System.out.println("fim ...");
                    break;
                case 1:
                    System.out.println("registar GA...");
                    GrupoAutomovelUI ga = new GrupoAutomovelUI();
                    ga.registarGA();
                    break;

                case 2:
                    System.out.println("LISTAR GA's...");
                    ListarGrupoAutoUI lgaUI = new ListarGrupoAutoUI();
                    lgaUI.listarGAs();
                    break;

                case 3:
                    System.out.println("REGISTAR AUTOMOVEIS...");
                    RegistarAutomovelUI raUI = new RegistarAutomovelUI();
                    raUI.registarAutomovel();
                    break;
                default:
                    System.out.println("opcao não reconhecida.");
                    break;
            }
        } while (opcao != 0);

    }

    private static int menu() {
        int option = -1;
        System.out.println("");
        System.out.println("=============================");
        System.out.println(" Rent a Car ");
        System.out.println("=============================\n");
        System.out.println("1.Registar Grupo Automóvel");
        System.out.println("2.Listar todos os Grupos Automóveis");
        System.out.println("3.Registar Automóveis");

        System.out.println("=============================");
        System.out.println("0. Sair\n\n");
        option = Console.readInteger("Por favor escolha opção");
        return option;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_ORM.apresentacao;

import isep.eapli.demo_orm.aplicacao.ListarGrupoAutoController;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.util.List;

/**
 *
 * @author Ferreira
 */
public class ListarGrupoAutoUI {
    
       private final ListarGrupoAutoController controller = new ListarGrupoAutoController();
       
         public void listarGAs() {
             List<GrupoAutomovel> listaGruposAuto = controller.listarGruposAuto();
             
             for (GrupoAutomovel grupoAutomovel : listaGruposAuto) {
                 System.out.println("Grupo Automóvel:");
                 System.out.println(grupoAutomovel);
             }
         }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_ORM.persistencia;
import isep.eapli.demo_orm.dominio.Automovel;



/**
 *
 * @author Ferreira
 */
public class AutomovelRepositorioJPAImpl extends JpaRepository<Automovel, Long> implements AutomovelRepositorio {

    @Override
    protected String persistenceUnitName() {
        return "DEMO_ORMPU";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_ORM.persistencia;

import isep.eapli.demo_orm.dominio.Automovel;
import java.util.List;

/**
 *
 * @author Ferreira
 */
public interface AutomovelRepositorio {
      /**
     * inserts an entity and commits
     *
     * @param entity
     * @return the persisted entity
     */
    public Automovel add(Automovel entity);

    /**
     * reads an entity given its ID
     *
     * @param id
     * @return
     */
    public Automovel findById(Long id);

    /**
     * Returns the List of all entities in the persistence store
     *
     * @return
     */
    public List<Automovel> findAll();
}

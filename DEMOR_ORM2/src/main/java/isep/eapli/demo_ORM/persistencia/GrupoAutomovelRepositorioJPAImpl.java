/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_ORM.persistencia;

import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.util.List;

/**
 *
 * @author eapli
 */
public class GrupoAutomovelRepositorioJPAImpl extends JpaRepository<GrupoAutomovel, Long> implements GrupoAutomovelRepositorio {

    @Override
    protected String persistenceUnitName() {
        return "DEMO_ORMPU";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.aplicacao;

import isep.eapli.demo_ORM.persistencia.GrupoAutomovelRepositorioJPAImpl;
import isep.eapli.demo_ORM.persistencia.JpaRepository;
import isep.eapli.demo_orm.dominio.GrupoAutomovel;
import java.util.List;

/**
 *
 * @author Ferreira
 */
public class ListarGrupoAutoController {
    
    public List<GrupoAutomovel> listarGruposAuto(){
        JpaRepository<GrupoAutomovel, Long> repo = new GrupoAutomovelRepositorioJPAImpl();
        return repo.findAll();
    }
    
}

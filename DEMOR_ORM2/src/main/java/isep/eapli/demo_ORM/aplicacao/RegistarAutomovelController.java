/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_ORM.aplicacao;

import isep.eapli.demo_ORM.persistencia.AutomovelRepositorioJPAImpl;
import isep.eapli.demo_ORM.persistencia.JpaRepository;
import isep.eapli.demo_orm.dominio.Automovel;
import java.util.List;

/**
 *
 * @author Ferreira
 */
public class RegistarAutomovelController {

    public Automovel registaAutomovel(String marca, String cor, int celindrada, int matricula, double km) {
        Automovel auto = new Automovel(marca, cor, celindrada, matricula, km);
        JpaRepository<Automovel, Long> repo = new AutomovelRepositorioJPAImpl();
        return repo.add(auto);
    }
}

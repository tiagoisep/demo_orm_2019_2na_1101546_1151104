/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import javax.persistence.*;

/**
 *
 * @author Ferreira
 */
@Entity
public class GrupoAutomovel {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    int id;
    String nome;
    int portas;
    String classe;

    public GrupoAutomovel(String nome, int portas, String classe) {
        this.nome = nome;
        this.portas = portas;
        this.classe = classe;
    }

    public void alterarClasse(String classe) {
        this.classe = classe;
    }

    public void alterarNumeroDePortas(int portas) {
        this.portas = portas;
    }

    @Override
    public String toString() {
        return "GrupoAutomovel{" + "id=" + id + ", nome=" + nome + ", portas=" + portas + ", classe=" + classe + '}';
    }

    private GrupoAutomovel() {
    }

}

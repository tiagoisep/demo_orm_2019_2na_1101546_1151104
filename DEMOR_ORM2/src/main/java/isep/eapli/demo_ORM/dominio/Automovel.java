/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package isep.eapli.demo_orm.dominio;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author anacerqueira
 */
@Entity
public class Automovel {

    String marca;
    String cor;
    int celindrada;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    int matricula;
    double km;

    public Automovel(String marca, String cor, int celindrada, int matricula, double km) {
        this.marca = marca;
        this.cor = cor;
        this.celindrada = celindrada;
        this.matricula = matricula;
        this.km = km;
    }
public Automovel(){
    
}
    public long devolveMatricula() {
        return matricula;
    }

    @Override
    public String toString() {
        return "Automovel{" + "marca=" + marca + ", cor=" + cor + ", celindrada=" + celindrada + ", matricula=" + matricula + '}';
    }

    public void alteraKm(double km) {
        this.km = km;
    }

}
